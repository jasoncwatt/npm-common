var spawn = require('child_process').spawn;

function processOutout(data, callback) {
    var messages = data.toString('utf8');
    var msgs = messages.split('\n');
    for (var m in msgs) {
        var msg = msgs[m];
        if (msg.trim() != "") {
            callback(msg)
        }
    }
}

/**
 *
 * @param command
 * @param path
 * @param callback (out, err, complete)
 */
exports.run = function (command, path, callback) {
    var args = command.split(' ');
    var app = this;
    var errors;
    var opts;
    var beforePre = false;

    command = args[0];
    args.shift();

    if (typeof console.prefix != "undefined") {
        beforePre = console.prefix;
    }
    console.indent++;
    console.prefix = false;
    console.log("Running " + command);

    if (typeof path == "string") {
        opts = {cwd: path};
    } else {
        opts = path;
    }
    var c = spawn(command, args, opts);
    c.stdout.on('data', function (data) {
        processOutout(data, function (message) {
            console.log(message);
            callback(message, false, false);
        });
    });

    c.stderr.on('data', function (data) {
        processOutout(data, function (message) {
            if (message.indexOf('fatal') >= 0) {
                console.error(message);
            } else {
                console.log(message);
            }
            callback(false, message, false);
        });
    });

    c.on('close', function (code) {
        console.debug('Exit Code: ' + code);
        console.indent--;
        console.prefix = beforePre;
        if (code != 0) {
            callback(false, code, true);
        } else {
            callback(false, false, true);
        }
    });
};