var colors = require('./ansi-256.js');
var util = require('util');

function _timestamp(msg) {
    if (console.timestamp) {
        var date = new Date();
        return date.getUTCFullYear() + '-' + (date.getUTCMonth() + 1) + '-' + date.getUTCDate() + ' ' + ((date.getUTCHours() < 10) ? '0' : '') + date.getUTCHours() + ':' + ((date.getUTCMinutes() < 10) ? '0' : '') + date.getUTCMinutes() + ':' + ((date.getUTCSeconds() < 10) ? '0' : '') + date.getUTCSeconds() + ' ';
    }
    return '';
}

function consoleControl(msg) {
    if (console.indent < 0 || console.indent === undefined || console.indent == null) {
        console.indent = 0;
    }
    var out = util.format.apply(this, arguments).split('\n');
    for (var i in out) {
        out[i] = _timestamp() + new Array((console.indent * 4)).join(' ') + out[i];
    }

    process.stdout.write(out.join("\n") + "\n");
}
console.indent = 0;
console.timestamp = false;
console.prefix = false;
console.log = consoleControl;

var normal = '\033[21m';
var bold = '\033[1m';
var italic = '\033[3m';
var underline = '\033[4m';

var error = function () {
    output(colors.bg.Rgbf(5, 0, 0) + colors.fg.RgbBf(5, 5, 5) + (console.prefix ? 'ERROR: ' : '') + util.format.apply(this, arguments));
};

var warn = function () {
    output(colors.bg.Rgbf(4, 2, 0) + colors.fg.RgbBf(5, 5, 5) + ' ' + (console.prefix ? 'WARNING: ' : '') + util.format.apply(this, arguments) + ' ');
};

var info = function () {
    output((console.prefix ? bold + 'INFO: ' + colors.reset : '') + util.format.apply(this, arguments) + ' ');
};

var notice = function () {
    output(colors.fg.Rgbf(0, 3, 5)  + (console.prefix ? bold + 'NOTICE: ' + colors.reset + colors.fg.standard[2] : '') + util.format.apply(this, arguments) + ' ');
};

var debug = function () {
    output(colors.fg.RgbBf(1, 1, 1) + (console.prefix ? bold + 'DEBUG: ' + normal : '') + util.format.apply(this, arguments) + ' ');
};

var exit = function () {
    var exit = arguments[arguments.length - 1];
    if (exit == parseInt(exit, 10)) {
        Array.prototype.pop.apply(arguments);
    } else {
        exit = 0;
    }
    error.apply(this, arguments);
    process.exit(exit);
};

function output(msg) {
    console.log(msg + colors.reset);
}

exports.log = consoleControl
exports.error = error;
console.error = error;

exports.warn = warn;
console.warn = warn;

exports.exit = exit;
console.exit = exit;

exports.info = info;
console.info = info;

exports.notice = notice;
console.notice = notice;

exports.debug = debug;
console.debug = debug;

exports.ok = console.ok = function () {
    output(colors.fg.RgbBf(0, 4, 0) + util.format.apply(this, arguments));
};
exports.alert = console.alert = function () {
    output(colors.fg.RgbBf(5, 2, 0) + util.format.apply(this, arguments));
};
exports.stage = console.stage = function () {
    console.log('');
    output(colors.bg.Rgbf(0, 2, 4) + colors.fg.RgbBf(5, 5, 5) + '          ' + util.format.apply(this, arguments) + '          ');
    console.log('');
};
exports.project = console.project = function () {
    console.log('');
    output(colors.bg.Rgbf(1, 0, 3) + colors.fg.RgbBf(5, 5, 5) + '          ' + util.format.apply(this, arguments) + '          ');
    console.log('');
};
exports.complete = console.complete = function () {
    console.log('');
    output(colors.bg.Rgbf(0, 4, 0) + colors.fg.RgbBf(5, 5, 5) + '          ' + util.format.apply(this, arguments) + '          ');
    console.log('');
};
exports.skip = console.skip = function () {
    output(colors.fg.Rgbf(0, 3, 5) + util.format.apply(this, arguments));
};
